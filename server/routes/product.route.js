import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import productCtrl from '../controllers/product.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(productCtrl.list)
  .post(validate(paramValidation.createProduct), productCtrl.create);

router.route('/:id')
  .get(productCtrl.get)
  .delete(productCtrl.remove);

router.param('id', productCtrl.load);

export default router;
