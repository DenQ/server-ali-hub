import ProductModel from '../models/product.model';

function load(req, res, next, id) {
  ProductModel.findById(id)
    .then((product) => {
      req.product = product; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

function list(req, res, next) {
  // const { limit = 50, skip = 0 } = req.query;
  ProductModel.findAll({
    attributes: ['id', 'name', 'url', 'price'],
  })
    .then(result => res.json(result))
    .catch(e => next(e));
}

function create(req, res, next) {
  const { name, url, price } = req.body;
  ProductModel.create({ name, url, price })
    .then(result => res.json(result))
    .catch(e => next(e));
}

function get(req, res) {
  return res.json(req.product);
}

function remove(req, res, next) {
  const product = req.product;
  ProductModel.destroy({
    where: {
      id: product.id,
    }
  })
    .then(deletedProduct => res.json(deletedProduct))
    .catch(e => next(e));
}


export default { list, create, get, remove, load };
