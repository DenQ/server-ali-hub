import Sequelize from 'sequelize';
import db from '../../db/index';

const Product = db.sequelize.define('product', {
  url: Sequelize.TEXT,
  name: Sequelize.TEXT,
  price: Sequelize.REAL,
}, {
  createdAt: false,
  updatedAt: false,
});

export default Product;
