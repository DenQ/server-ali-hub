var Sequelize = require('sequelize');
var env       = process.env.NODE_ENV || 'development';
import Config from '../../db/config/config.json';

const config = Config[env];
var db        = {};

var sequelize = new Sequelize(config.database, config.username, config.password, config);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
